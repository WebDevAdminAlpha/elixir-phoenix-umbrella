# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
use Mix.Config

# Configure Mix tasks and generators
config :elixir_phoenix,
  ecto_repos: [ElixirPhoenix.Repo]

config :elixir_phoenix_web,
  ecto_repos: [ElixirPhoenix.Repo],
  generators: [context_app: :elixir_phoenix]

# Configures the endpoint
config :elixir_phoenix_web, ElixirPhoenixWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Bj1agSsrFZb7NMoi1boa1GOmyU/6+oeGFP/3lc//FOiqAWFAaMG7ZN6iCjj9AzMa",
  render_errors: [view: ElixirPhoenixWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ElixirPhoenixWeb.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
